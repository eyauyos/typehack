﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsController : MonoBehaviour
{
    public GameObject dialog;
    public GameObject tittle;
    public GameObject controls;
    public Button next;
    public Button start;

    public List<GameObject> tutorials;
    private int index=1;

    private void Start()
    {
        next.onClick.AddListener(() =>
        {
            tutorials[index].SetActive(true);
            index++;
            if (index >= tutorials.Count)
            {
                next.gameObject.SetActive(false);
                start.gameObject.SetActive(true);
            }
        });

        start.onClick.AddListener(() =>
        {
            controls.SetActive(false);
            dialog.SetActive(true);
            tittle.SetActive(true);
        });
    }
}
