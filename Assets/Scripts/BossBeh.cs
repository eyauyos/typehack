﻿using System.Collections;
using UnityEngine;

public class BossBeh : MonoBehaviour
{
    public float delayTime = 5;
    public Player player;
    public bool start;
    public EnemiePathController path;  
    public Position myPosition;
    public Stage myStage;
    public bool stopfire;
    public Animator boss_anim;
    public AudioSource damage;
    public AudioSource s_patt2;
    public AudioSource s_patt1;
    [SerializeField]
    private PatternController patt;
    private float delayMiddle = 5;
    private float delayTop = 5;
    private float delayLR = 5;
    private bool goLeft;
    private bool goRight;
    private bool move;
    private bool p_patt1;
    private bool p_patt2;
    private int count;
    private void Awake()
    {
        path = FindObjectOfType<EnemiePathController>();
        patt = FindObjectOfType<PatternController>();
        path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        p_patt1=false;
        p_patt2=false;
        count = 0;
    }
    void Start()
    {
        player = FindObjectOfType<Player>();
        start = true;
        goRight = true;
        goLeft = true;
        move = false;
        stopfire = false;
        myStage = Stage.First;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(TakingDamage());
        }
        switch(myStage){
            case Stage.First:
                patt.stage1 = true;
                StartCoroutine(Stage_1());
                break;
            case Stage.Second:
                patt.stage1 = false;
                patt.stage2 = true;
                StartCoroutine(Stage_2());
                break;
            case Stage.Cycle:
                patt.stage2 = false;
                StartCoroutine(Cycle());
                break;
            case Stage.None:
                break;
        }
    }
    public IEnumerator Stage_1()
    {
        if (start)
        {
            myPosition = Position.None;
            yield return new WaitForSeconds(delayTime);
            myPosition = Position.None;
            StartCoroutine(MovingTop());
            yield return new WaitForSeconds(delayTop);
            start = false;
            goLeft = true;
            goRight = true;
        }
        if (goLeft && goRight && !start)
        {
            myPosition = Position.None;
            goLeft = false;
            goRight = false;
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
            yield return new WaitForSeconds(delayTop);
            StartCoroutine(Left());
            yield return new WaitForSeconds(2*delayLR);
            StartCoroutine(Rigth());
            yield return new WaitForSeconds(delayLR);
            move = true;
        }
        else if(move)
        {
            yield return new WaitForSeconds(delayLR);
            myPosition = Position.None;
            StartCoroutine(Middle());
            if (count==0)
            {
                s_patt1.gameObject.SetActive(true);
                count = 1;
            }
            myStage = Stage.None;
            yield return new WaitForSeconds(2*delayMiddle);
            patt.fire = false;
            myPosition = Position.None;
            s_patt1.gameObject.SetActive(false);
            StartCoroutine(Top());
            myPosition = Position.None;
            myStage = Stage.Second;
            yield return new WaitForSeconds(delayTop);            
            patt.fire = true;
        }
    }
    public IEnumerator Stage_2()
    {
        yield return new WaitForSeconds(delayTime);
        path.m_Velocity = 7;
        if (!start)
        {
            s_patt2.gameObject.SetActive(true);
            myPosition = Position.None;
            start = true;
            path.SetPathList(path.m_Paths[0], 1);
            path.m_StateMovement = EnemiePathController.StateMovement.moving;
            yield return new WaitForSeconds(20);
            s_patt2.gameObject.SetActive(false);
            myStage = Stage.Cycle;
        }
        else if (transform.position == path.GetPointPosition(0))
        {
            yield return new WaitForSeconds(3);
            path.m_TypeTour = EnemiePathController.TypeTour.roundAndTrip;
        }

    }
    public IEnumerator Cycle()
    {
        if (path.m_StateMovement == EnemiePathController.StateMovement.moving)
        {
            transform.position = path.GetPointPosition(3);
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        }
        else
        {
            yield return new WaitForSeconds(3);
            transform.position = path.GetPointPosition(3);
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        }
        myPosition = Position.None;
        myStage = Stage.First;
        patt.cycle = true;
        patt.stage1 = true;
        patt.fire = true;
        move = false;
        yield return new WaitForSeconds(delayTime);
    }
    public IEnumerator Left()
    {
        transform.position = path.GetPointPosition(2);
        myPosition = Position.Left;
        goLeft = false;
        yield return new WaitForSeconds(delayLR);

    }
    public IEnumerator Rigth()
    {
        transform.position = path.GetPointPosition(1);
        myPosition = Position.Rigth;
        goRight = false;
        yield return new WaitForSeconds(delayLR);

    }
    public IEnumerator Middle()
    {
        transform.position = path.GetPointPosition(3);
        myPosition = Position.Middle;
        patt.fire = true;
        yield return new WaitForSeconds(delayMiddle);
    }
    public IEnumerator Top()
    {
        transform.position = path.GetPointPosition(0);
        myPosition = Position.Top;
        if (myStage == Stage.Second)
        {
            myStage = Stage.Cycle;
        }
        yield return new WaitForSeconds(delayTop);
    }
    public IEnumerator MovingTop()
    {
        path.SetPathList(path.m_Paths[0], 0);
        path.m_StateMovement = EnemiePathController.StateMovement.moving;
        if (transform.position == path.GetPointPosition(0))
        {
            myPosition = Position.Top;
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        }
        yield return new WaitForSeconds(delayTop);
    }
    public IEnumerator MovingLeft()
    {
        path.SetPathList(path.m_Paths[0], 2);
        path.m_StateMovement = EnemiePathController.StateMovement.moving;
        if (transform.position == path.GetPointPosition(2))
        {
            myPosition = Position.Left;
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        }
        yield return new WaitForSeconds(delayLR);
    }
    public IEnumerator MovingRight()
    {
        path.SetPathList(path.m_Paths[0], 1);
        path.m_StateMovement = EnemiePathController.StateMovement.moving;
        if (transform.position == path.GetPointPosition(1))
        {
            myPosition = Position.Rigth;
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        }
        yield return new WaitForSeconds(delayLR);
    }
    public IEnumerator MovingMiddle()
    {
        path.SetPathList(path.m_Paths[0], 3);
        path.m_StateMovement = EnemiePathController.StateMovement.moving;
        if (transform.position == path.GetPointPosition(3))
        {
            myPosition = Position.Middle;
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
        }
        yield return new WaitForSeconds(delayMiddle);
    }
    public enum Position
    {
        Middle, Left, Rigth, Top, None
    }
    public enum Stage
    {
        First, Second, Cycle, None
    }

    public void TakingDamageCoroutine()
    {
        StartCoroutine(TakingDamage());
    }

    public IEnumerator TakingDamage()
    {
        if (s_patt1.isPlaying)
        {
            s_patt1.gameObject.SetActive(false);
            p_patt1 = true;
        }
        else if (s_patt2.isPlaying)
        {
            s_patt2.gameObject.SetActive(false);
            p_patt2 = true;
        }
        damage.gameObject.SetActive(true);
        stopfire = true;
        boss_anim.SetBool("Damaged", true);
        if (path.m_StateMovement == EnemiePathController.StateMovement.moving)
        {
            path.m_StateMovement = EnemiePathController.StateMovement.stopped;
            yield return new WaitForSeconds(2); 
            if (myStage == Stage.Second)
            {
                patt.Pattern2.gameObject.SetActive(true);
            }
            path.m_StateMovement = EnemiePathController.StateMovement.moving;
            boss_anim.SetBool("Damaged", false);
            yield return new WaitForSeconds(0.5f);
            if(p_patt1 && myPosition == Position.Middle)
            {
                s_patt1.gameObject.SetActive(true);
            }
            if (p_patt2 && myPosition != Position.Middle)
            {
                s_patt2.gameObject.SetActive(true);
            }
            p_patt1 = false;
            p_patt2 = false;
            stopfire = false;
            damage.gameObject.SetActive(false);
        }
        else if(path.m_StateMovement == EnemiePathController.StateMovement.stopped) 
        {
            yield return new WaitForSeconds(2);
            boss_anim.SetBool("Damaged", false);
            yield return new WaitForSeconds(0.5f);
            stopfire = false;
            if (p_patt1 && myPosition == Position.Middle)
            {
                s_patt1.gameObject.SetActive(true);
            }
            if (p_patt2 && myPosition != Position.Middle)
            {
                s_patt2.gameObject.SetActive(true);
            }
            damage.gameObject.SetActive(false);
        }
    }
}
