﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class IntroTextController : MonoBehaviour
{
    public TextMeshProUGUI inputText; 
    public TextMeshProUGUI templateText;
    [TextArea]
    public string text_intro;
    [TextArea]
    public string text_firstStage;
    [TextArea]
    public string text_SecondStage;
    [TextArea]
    public string text_ThirdStage;
    
    private string currentText;
    private int index;
    [SerializeField] private int numberMistakes;

    [SerializeField] private Animator animator;

    [SerializeField] private List<string> textArray;
    
    private int textsIndex = 0;
    
    public delegate void CompleteCreation();
    public static event CompleteCreation OnCompleteCreation;
    void Start()
    {
        SetInitialText();
    }

    private void SetInitialText()
    {
        textArray.Add(text_intro);
        textArray.Add(text_firstStage);
        textArray.Add(text_SecondStage);
        textArray.Add(text_ThirdStage);

        templateText.text = textArray[0];
        currentText = textArray[0];
    }

    void Update()
    {
        ReadInput();
        CheckTextFinished();
        CheckNumberMistakes();
    }

    private void CheckTextFinished()
    {
        if (currentText.Length == index)
        {
            textsIndex++;
            if(textsIndex == textArray.Count) {
                OnCompleteCreation?.Invoke();
            } else {
                switch (textsIndex)
                {
                    case 1:
                        animator.SetBool("introToFirst", true);
                        break;
                    case 2:
                        animator.SetBool("firstToSecond", true);
                        break;
                    case 3:
                        animator.SetBool("secondToThird", true);
                        break;
                    case 4:
                        animator.SetBool("thirdToFinal", true);
                        break;         
                    default:
                        break;
                }
                currentText = textArray[textsIndex];
                templateText.text = textArray[textsIndex];
                inputText.text = "";
                index = 0;
            }
        }
    }

    private void CheckNumberMistakes()
    {
        if (numberMistakes > 5)
        {
            Debug.Log("Reinicio");
            inputText.text = "";
            numberMistakes = 0;
            index = 0;
        }
    }

    private void ReadInput()
    {
        if (Input.anyKeyDown)
        {
            if (Input.inputString.Length > 0)
            {
                validateCharacter(Input.inputString[0]);
                
            }
        }
    }

    private void validateCharacter(char c) 
    {
        if(c.ToString().ToUpper() == currentText[index].ToString().ToUpper()) 
        {
            index++;
            inputText.text += c;
        } else if((c == '\n' || c == '\r') && currentText[index] == '\n')
        {
            index++;
            inputText.text += '\n';
        }
        else 
        {
            numberMistakes++;
        }
    }
}
