﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EnemiePathController : MonoBehaviour
{
    
    public Transform[] m_Paths;
    public TypeTour m_TypeTour;
    public StateMovement m_StateMovement;
    public float m_Velocity;
    public PatternController pattC;
    public bool move = true;
    private Transform m_Path;
    //[SerializeField]
    private List<Transform> PathPoints;
    //[SerializeField]
    private int currentIndex;
    private int direction=1;

    public enum TypeTour
    {
        cycle,
        roundAndTrip
    }

    public enum StateMovement
    {
        stopped,
        moving
    }

    private void Awake()
    {
        pattC = FindObjectOfType<PatternController>();
        SetPathList(m_Paths[0]);
    }

    
    void Update()
    {
        if (m_Path != null && move)
        {

            switch (m_StateMovement)
            {
                case StateMovement.moving:
                    {

                        //if (m_Path == null)
                        //    m_StateMovement = StateMovement.stopped;
                        transform.position = Vector3.MoveTowards(transform.position, PathPoints[currentIndex].transform.position, Time.deltaTime * m_Velocity);

                        if (transform.position == PathPoints[currentIndex].transform.position)
                        {
                            switch (m_TypeTour)
                            {   
                                case TypeTour.cycle:
                                    {
                                        if (currentIndex <= 0)
                                            direction = 1;

                                        currentIndex += direction;

                                        if (currentIndex > PathPoints.Count - 1)
                                            currentIndex = 0;


                                    }
                                    break;
                                case TypeTour.roundAndTrip:
                                    {

                                        if (currentIndex <= 0)
                                            direction = 1;
                                        if (currentIndex >= PathPoints.Count - 1)
                                            direction = -1;

                                        currentIndex += direction;

                                    }
                                    break;
                            }

                        }

                    }
                    break;


                case StateMovement.stopped:
                    {
                        //STOP
                    }
                    break;
            }
        }
    }




    #region PUBLIC_METHODS
    public void SetPathList(Transform newPath, int startIndex)
    {
        
        currentIndex = startIndex;
        PathPoints = new List<Transform>();
        PathPoints.Clear();
        m_Path = newPath;
        int childCount = newPath.transform.childCount;

        if (startIndex > childCount - 1)
        {
            currentIndex = childCount-1;
            Debug.LogWarning("Índice usado es mayor que la cantidad de elementos del path, índice reemplazado automáticamente por el último elemento del path");
        }
            


        for (int i = 0; i < childCount; i++)
        {
            PathPoints.Add(newPath.GetChild(i));
        }
    }

    public void SetPathList(Transform newPath)
    {
        currentIndex = 0;
        PathPoints = new List<Transform>();
        PathPoints.Clear();
        m_Path = newPath;
        int childCount = newPath.transform.childCount;

        for (int i = 0; i < childCount; i++)
        {
            PathPoints.Add(newPath.GetChild(i));
        }
    }
    public Vector3 GetPointPosition(int index)
    {

        Vector3 pos = PathPoints[index].transform.position;
        return pos;
    }
    #endregion


    #region PRIVATE_METHODS



    #endregion
}

