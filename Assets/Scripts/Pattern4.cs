﻿using System.Collections.Generic;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Pattern4 : MonoBehaviour
{
    // Start is called before the first frame update
    public float freqBullet;
    public float lifeBullet;
    public float Impulse=10;
    public int numberBullets;
    public GameObject player;
    public PatternController pattC;
    public EnemiePathController boss;
    [SerializeField]
    private List<Bullet> temp_bullets;
    private PatternController p_contr;
    private Vector3 player_pos;
    private Rigidbody2D rbd;
    private void Awake()
    {
        boss = FindObjectOfType<EnemiePathController>();
        pattC = FindObjectOfType<PatternController>();
        p_contr = FindObjectOfType<PatternController>();
        rbd = GetComponent<Rigidbody2D>();
        gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        boss.move = false;
        temp_bullets = new List<Bullet>(numberBullets);
        player_pos = player.transform.position;
    }
    void Start()
    {
        
    }

    
    private void FixedUpdate()
    {
        if (!((transform.position - player_pos).magnitude < 0.01f))
        {
            //transform.position = Vector3.MoveTowards(transform.position, player_pos, Time.fixedDeltaTime * 5);
            rbd.MovePosition(Vector3.MoveTowards(transform.position, player_pos, Time.fixedDeltaTime * 5));
        }
        else
        {
            BulletExplotion();
            transform.position = p_contr.transform.position;
            boss.move = true;
            gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    public void BulletExplotion()
    {
        float acc = 0;
        for (int i = 0; i < numberBullets; i++)
        {
            temp_bullets.Add(BulletPool.Instance.BulletPoolInstantiate(lifeBullet));
            temp_bullets[i].transform.position = transform.position;
temp_bullets[i].transform.eulerAngles = new Vector3(0, 0, acc);
            acc += (360 / numberBullets);
        }
        foreach (var bullet in temp_bullets)
        {
            bullet.m_Rbd.velocity = Vector3.zero;
            bullet.m_Rbd.AddForce(bullet.transform.right * Impulse, ForceMode2D.Impulse);
        }
    }
    public void OnDisable()
    {
        pattC.patt4 = false;
    }
}
