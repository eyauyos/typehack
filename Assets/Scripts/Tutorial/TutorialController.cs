﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    
    [Header("UI Actions")]
    public Image WakeUpSit;
    public Image WakeUpSleep;
    public Image Sleep;
    public Image Controls;
    public Image SitPC;
    public GameObject dialogEntryPC;
    public GameObject Bedroom;
    public GameObject PC;
    public float bedroomDistance;
    public float pcDistance;
    public Animator RoomAnimator;

    public string wakeUpSit;
    public string wakeUpSleep;
    public string sitPC;
    public string sleep;

    public TPController player;
    public bool wakeUpAction=true;
    public bool isSleepAction=true;
    public bool isSitPCAction = false;
    public bool wakeUpSleepAction = true;

    public delegate void FinishTutorial();
    public static event FinishTutorial OnFinishTutorial;

    void Update()
    {
        if (WakeUpSit.gameObject.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                RoomAnimator.SetTrigger(wakeUpSit);
                WakeUpSit.gameObject.SetActive(false);
            }
        }

        if (WakeUpSleep.gameObject.activeSelf&& wakeUpSleepAction)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                RoomAnimator.SetTrigger(wakeUpSleep);
                WakeUpSleep.gameObject.SetActive(false);
                Controls.gameObject.SetActive(false);
                player.active = false;
                wakeUpSleepAction = false;
            }
        }

        if (Sleep.gameObject.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                RoomAnimator.SetTrigger(sleep);
                Sleep.gameObject.SetActive(false);
                Controls.gameObject.SetActive(false);
                player.active = false;
                isSleepAction = false;
                Sleep.gameObject.SetActive(false);
            }
            
        }

        if (SitPC.gameObject.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                RoomAnimator.SetTrigger(sitPC);
                SitPC.gameObject.SetActive(false);
                Controls.gameObject.SetActive(false);
                player.active = false;
                isSitPCAction = false;
            }

        }

        float distance = (Bedroom.transform.position - player.transform.position).magnitude;
        if (isSleepAction)
        {
            if (distance < bedroomDistance)
            {
                Sleep.gameObject.SetActive(true);
            }
            else
            {
                Sleep.gameObject.SetActive(false);
            }
        }

        float distancePC = (PC.transform.position - player.transform.position).magnitude;
        if (isSitPCAction)
        {
            if (distancePC < pcDistance)
            {
                SitPC.gameObject.SetActive(true);
            }
            else
            {
                SitPC.gameObject.SetActive(false);
            }
        }

    }
    public void EntryPCDialog()
    {
        dialogEntryPC.SetActive(true);

    }
    public void FinishTutorialEvent()
    {
        OnFinishTutorial?.Invoke();
    }

    public void SleepTransition()
    {
        player.gameObject.SetActive(false);
    }

    public void SitPCTransition()
    {
        player.gameObject.SetActive(false);
    }

    public void ActiveWakeUpSitAction()
    {
        if(wakeUpAction)
        WakeUpSit.gameObject.SetActive(true);
        wakeUpAction = false;
    }

    public void ActiveSleepAction()
    {
        Sleep.gameObject.SetActive(true);
    }

    public void ActiveWakeUpSleepAction()
    {
        if(wakeUpSleepAction)
        WakeUpSleep.gameObject.SetActive(true);
        isSitPCAction = true;

    }

    public void ActiveControlsAction()
    {
        Controls.gameObject.SetActive(true);
        player.active = true;
        player.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        
    }


}
