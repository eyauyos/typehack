﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pattern3 : MonoBehaviour
{
    // Start is called before the first frame update
    public float Impulse;
    public int numberBullets;
    public int currentBulletsNumber;
    public float endAngle = 360;
    public float startAngle = 0;
    public float freqBullet;
    public float lifeBullet;
    [SerializeField]
    public float angleStep;
    private float angle;
    private List<Bullet> temp_bullets;

    private bool otherAngle;
    public void Awake()
    {

        gameObject.SetActive(false);
    }
    public void OnEnable()
    {
        currentBulletsNumber = numberBullets;
        otherAngle = !otherAngle;
        temp_bullets = new List<Bullet>(numberBullets + 1);
        angleStep = (endAngle - startAngle) / numberBullets;
        if (otherAngle)
        {
            angle = startAngle;
            numberBullets= currentBulletsNumber;
        }
        else
        {
            angle = startAngle+angleStep/2;
            numberBullets--;
        }
        
        StartCoroutine(FireCoroutine(numberBullets));
    }
    private IEnumerator FireCoroutine(int numberBullets)
    {
        for (int i = 0; i < numberBullets+1; i++)
        {
            temp_bullets.Add(BulletPool.Instance.BulletPoolInstantiate(lifeBullet));
        }
        for (int i = 0; i < numberBullets+1; i++)
        {            
            temp_bullets[i].transform.position = transform.position;
            temp_bullets[i].transform.eulerAngles = new Vector3(0, 0, angle);
            temp_bullets[i].transform.position += temp_bullets[i].transform.right * 2f;
            angle += angleStep;
        }
        FireFunction();
        gameObject.SetActive(false);
        yield return new WaitForSeconds(freqBullet);
    }
    public void FireFunction()
    {
        foreach (var bullet in temp_bullets)
        {
            bullet.m_Rbd.velocity = Vector3.zero;
            //bullet.gameObject.SetActive(true);
            bullet.m_Rbd.AddForce(bullet.transform.right * Impulse, ForceMode2D.Impulse);
        }
    }
    /*private void OnDisable()
    {
        StopAllCoroutines();
    }*/
}
