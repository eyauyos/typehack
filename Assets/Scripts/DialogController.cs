﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class DialogController : MonoBehaviour
{
    public enum Character {Player, IA};
    [System.Serializable]
    public struct DialogBox {
        [Multiline]
        public string dialog;
        public Character character;
    };
    public List<DialogBox> characterList = new List<DialogBox>();
    private int indexDialogs;

    private bool continueButton;
    private bool firstTime;
    [SerializeField] TextMeshProUGUI boxDialogText;
    [SerializeField] GameObject messageText;
    bool blinkMessage;

    [SerializeField] 
    IEnumerator blinkCoroutine;

    [SerializeField] GameObject characterHead;
    [SerializeField] Sprite IA_HEAD_SPRITE;
    [SerializeField] Sprite PLAYER_HEAD_SPRITE;

    public bool isFinished = false;
    void Start() 
    {
        setInitialValues(); 
        blinkCoroutine = blinkText(0.5f);        
    }


    void OnEnable() {
        setInitialValues(); 
        blinkCoroutine = blinkText(0.5f);
    }
    void Update()
    {
        PlayDialog();
    }

    private void PlayDialog()
    {
        if ((Input.GetKeyDown(KeyCode.Space) && continueButton) || firstTime)
        {
            if(indexDialogs < characterList.Count) {
                if(characterList[indexDialogs].character == Character.Player) {
                    characterHead.GetComponent<Image>().sprite = PLAYER_HEAD_SPRITE;
                }
                else if(characterList[indexDialogs].character == Character.IA) {
                    characterHead.GetComponent<Image>().sprite = IA_HEAD_SPRITE;
                }
                firstTime = false;
                StopBlinkingContinueMessage();
                StartCoroutine(typeDialogEffect(0.05f));
            }
            else {
                isFinished = true;
                gameObject.SetActive(false);
            }
            
        }
    }

    private void setInitialValues() {
        indexDialogs = 0;
        continueButton = true;
        firstTime = true;
        blinkMessage = false;
    }

    private void StopBlinkingContinueMessage()
    {
        messageText.SetActive(false);
        blinkMessage = false;
        StopCoroutine(blinkCoroutine);
    }

    IEnumerator typeDialogEffect(float delay) {
        int indexString = 0;
        boxDialogText.text = "";
        continueButton = false;

        while(true) {
            if(indexString < (characterList[indexDialogs].dialog).Length){
                yield return new WaitForSeconds(delay);
                boxDialogText.text += (characterList[indexDialogs].dialog)[indexString];
                indexString++;
            }
            else {
                continueButton = true;
                indexDialogs++;
                StartCoroutine(blinkCoroutine);
                break;
            }
        }
    }

    IEnumerator blinkText(float delay) {
        while(true) {
            yield return new WaitForSeconds(delay);
            blinkMessage = !blinkMessage;
            messageText.SetActive(blinkMessage);
        }
    } 
}
