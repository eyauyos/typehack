﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Bullet : MonoBehaviour
{
    public Rigidbody2D m_Rbd;
    public float m_LifeBullet;

    private void Awake()
    {
        m_Rbd = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        if(m_LifeBullet>0)
            Invoke("DestroyBulletPool", m_LifeBullet);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    public void DestroyBulletPool()
    {
        gameObject.SetActive(false);
    }
    public void velReset()
    {
        m_Rbd.velocity = Vector2.zero;
    }
}
