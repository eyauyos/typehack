﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class AppManager : MonoBehaviour
{
    public static AppManager Instance;


    public string GameScene;
    public string TutorialScene;
    public GameState gameState;

    private AudioSource[] audioSources;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        IntroTextController.OnCompleteCreation += LoadTutorialScene;

        TutorialController.OnFinishTutorial += LoadGameScene;


    }
    private void LoadGameScene()
    {
        SceneManager.LoadScene(GameScene);
    }

    private void LoadTutorialScene()
    {
        SceneManager.LoadScene(TutorialScene);
    }

    public void PauseGame()
    {
        gameState = GameState.pause;
        Time.timeScale = 0;

        PauseGeneralAudio();

    }

    private void PauseGeneralAudio()
    {
        audioSources = FindObjectsOfType<AudioSource>();

        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].Pause();
        }

    }

    private void PlayGeneralAudio()
    {
        audioSources = FindObjectsOfType<AudioSource>();

        for(int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].Play();
        }
    }

    public void ResumeGame()
    {
        gameState = GameState.inGame;
        Time.timeScale = 1;
        PlayGeneralAudio();
    }

    public enum GameState
    {
        inGame,
        pause
    }

    private void OnDestroy()
    {
        IntroTextController.OnCompleteCreation -= LoadGameScene;

        TutorialController.OnFinishTutorial -= LoadTutorialScene;
    }
}
