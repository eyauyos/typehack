﻿using System;
using System.Collections;
using UnityEngine;

public class PatternController : MonoBehaviour
{
    
    public bool patt4 = false;
    public bool fire=true;
    public bool stage1 = true;
    public bool stage2 = false;
    public bool cycle = false;
    public int extrabullets = 18;   
    public Pattern1 Pattern1;
    public Pattern2 Pattern2;
    public Pattern3 Pattern3;
    public Pattern4 Pattern4;
    public AudioSource b_sound;
    [SerializeField]
    private BossBeh boss;
    private int cont = 0;

    private void OnEnable()
    {
        boss = FindObjectOfType<BossBeh>();
        transform.position = boss.transform.position;
        Pattern1.gameObject.transform.position = transform.position;
        Pattern2.gameObject.transform.position = transform.position;
        Pattern3.gameObject.transform.position = transform.position;
        Pattern4.gameObject.transform.position = transform.position;
        fire = true;
        b_sound.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (stage1)
        {
            if (boss.myPosition == BossBeh.Position.Top) //boss.top
            {
                if (fire && !(boss.myPosition == BossBeh.Position.Middle) && !boss.stopfire)//boss.middle
                {
                    Pattern3.startAngle = 200;
                    Pattern3.endAngle = 340;
                    Pattern3.lifeBullet =5;
                    Pattern3.numberBullets = 10;
                    if (cycle)
                    {
                        Pattern3.startAngle = 200;
                        Pattern3.endAngle = 340;
                        Pattern3.lifeBullet = 3;
                        Pattern3.numberBullets = 16;
                    }
                    StartCoroutine(Pat3());
                }
            }
            else if (boss.myPosition == BossBeh.Position.Left)//boss.left
            {
                if (fire && !boss.stopfire)
                {
                    Pattern3.startAngle = 60;
                    Pattern3.endAngle = -60;
                    Pattern3.lifeBullet = 4;
                    Pattern3.numberBullets = 10;
                    if (cycle)
                    {
                        Pattern3.startAngle = 60;
                        Pattern3.endAngle = -60;
                        Pattern3.lifeBullet = 4;
                        Pattern3.numberBullets = 16;
                    }
                    StartCoroutine(Pat3());
                }

            }
            else if (boss.myPosition == BossBeh.Position.Rigth)//boss.right
            {
                if (fire && !boss.stopfire)
                {
                    Pattern3.startAngle = 110;
                    Pattern3.endAngle = 250;
                    Pattern3.lifeBullet = 4;
                    Pattern3.numberBullets = 10;
                    if (cycle)
                    {
                        Pattern3.startAngle = 110;
                        Pattern3.endAngle = 250;
                        Pattern3.lifeBullet = 4;
                        Pattern3.numberBullets = 16;
                    }
                    StartCoroutine(Pat3());
                }
            }
            else if (boss.myPosition == BossBeh.Position.Middle)//boss.middle
            {
                if (fire && !boss.stopfire)
                {
                    if (cycle)
                    {
                        Pattern1.rotationVel = 200;
                        Pattern1.freqBullet = (float)0.07;
                    }                    
                    StartCoroutine(Pat1());
                }
                else if(boss.stopfire)
                {
                    Pattern1.gameObject.SetActive(false);
                }
            }
        }
        if (stage2 && !boss.stopfire)
        {
            if (cycle)
            {
                Pattern4.Impulse = 15;
                Pattern4.numberBullets = 24;
            }
            StartCoroutine(Pat2());
        }
        else
        {
            Pattern2.gameObject.SetActive(false);
        }
    }
    private IEnumerator Pat3()
    {  
        Pattern3.gameObject.SetActive(true);       
        fire = false;
        if (cont==1)
        {
            b_sound.gameObject.SetActive(true);
        }
        else
        {
            cont = 1;
        } 
        Pattern3.gameObject.SetActive(false);
        yield return new WaitForSeconds(Pattern3.freqBullet);
        fire = true;
        b_sound.gameObject.SetActive(false);
    }
    private IEnumerator Pat1()
    {
        Pattern1.gameObject.SetActive(true);
        fire = false;
        yield return new WaitForSeconds(boss.delayTime);
        Pattern1.gameObject.SetActive(false);
        StopAllCoroutines();
        fire = true;
    }
    private IEnumerator Pat2()
    {
        if (fire)
        {
            Pattern2.gameObject.SetActive(true);
            fire = false;
        }
        else
        {
            if ((transform.position == boss.path.GetPointPosition(1) ||
                transform.position == boss.path.GetPointPosition(2) ||
                transform.position == boss.path.GetPointPosition(3)) && !boss.stopfire)
            {
                var random = new System.Random(Environment.TickCount);
                var value = (float)random.Next(0, 10)/10;
                yield return new WaitForSeconds(1+value);
                Pattern4.gameObject.SetActive(true);
            }
        }        
        yield return new WaitForSeconds(boss.delayTime);
    }
}
