﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pattern2 : MonoBehaviour
{
    // Start is called before the first frame update
    public float lifeBullet;
    public float freqBullet;
    public GameObject player;
    private void Awake()
    {
        gameObject.SetActive(false);
        player=FindObjectOfType<Player>().gameObject;
    }
    private void OnEnable()
    {
        transform.localEulerAngles = new Vector3(0, 0, 0);
        StartCoroutine(Fire02());
    }
    private void Start()
    {
       
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private IEnumerator Fire02()
    {
        while (true)
        {
            Follow_p();
            Bullet temp_Bullet = BulletPool.Instance.BulletPoolInstantiate(lifeBullet);
            temp_Bullet.transform.position = transform.position;
            temp_Bullet.m_Rbd.AddForce(transform.right * 5, ForceMode2D.Impulse);
            yield return new WaitForSeconds(freqBullet);
        }
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
    public void Follow_p()
    {
        transform.right = (player.transform.position - transform.position).normalized;
    }
}
