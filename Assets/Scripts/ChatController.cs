﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChatController : MonoBehaviour
{
    [SerializeField] List<string> chatTexts;
    [SerializeField] List<GameObject> messagesBoxes;
    [SerializeField] TextMeshProUGUI boxText;
    private int indexChat;
    private bool sendButton;
    private bool firstTime ;
    [SerializeField] GameObject EnterText;
    private bool blinkMessage;
    IEnumerator blinkCoroutine;
    private bool writeText;
    private bool available;

    public bool isFinished = false;

    [SerializeField] AudioClip typeSound;
    private AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        for(int i = 0; i < chatTexts.Count; i++) {
            messagesBoxes[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = chatTexts[i];
        }
    }
    void OnEnable() {
        blinkCoroutine = blinkText(0.5f);
        indexChat=0;
        sendButton = false;
        firstTime = true;
        writeText = false;
        available = false;
    }

    void OnDisable() {
        for(int i = 0; i < chatTexts.Count; i++) {
            messagesBoxes[i].SetActive(false);
        }
    }
    void Update()
    {
        Debug.Log(indexChat);
        Debug.Log(available);
        SendText();   
    }

    private void SendText() {
        if((Input.GetKeyDown(KeyCode.Return) && sendButton) || firstTime || writeText) {
            if(indexChat < chatTexts.Count) {
                firstTime = false;
                if(indexChat%2==0 && available) {
                    messagesBoxes[indexChat-1].SetActive(true);
                    available = false;
                }
                StopBlinkingEnterText();
                StartCoroutine(typeMessage()); 
            }
            else {
                if(indexChat%2==0 && available) { 
                    messagesBoxes[indexChat-1].SetActive(true);
                }
                boxText.text = "Escribe aqui ...";
                Debug.Log("ACABO PE");
                isFinished = true;
            }
        }
    }
    IEnumerator typeMessage() {
        int indexString = 0;
        boxText.text = "Escribe aqui ...";
        string message = chatTexts[indexChat];
        sendButton = false;
        if(indexChat%2 == 1) {
            writeText = false;
            boxText.text = "";
            audioSource.PlayOneShot(typeSound);
            while(true) {
                if(indexString < message.Length) {
                    yield return new WaitForSeconds(0.1f);
                    boxText.text += message[indexString];
                    indexString++;
                } else {
                    sendButton = true;
                    indexChat++;
                    blinkCoroutine = blinkText(0.5f);
                    StartCoroutine(blinkCoroutine);
                    break;
                }
            }
        } else {
            sendButton = false;
            Debug.Log("ETE SECH");
            yield return new WaitForSeconds(1f);
            messagesBoxes[indexChat].SetActive(true);
            indexChat++;
            sendButton = true;
            yield return new WaitForSeconds(1f);
            writeText = true;
        }
        
    }

    IEnumerator blinkText(float delay) {
        Debug.Log("blinking");
        audioSource.Stop();
        available = true;
        while(true) {
            yield return new WaitForSeconds(delay);
            blinkMessage = !blinkMessage;
            EnterText.SetActive(blinkMessage);
        }
    }

    private void StopBlinkingEnterText()
    {
        EnterText.SetActive(false);
        blinkMessage = false;
        StopCoroutine(blinkCoroutine);
        available = false;
    }
}
