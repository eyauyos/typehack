﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    public static BulletPool Instance;
    
    public GameObject bulletPrefab;


    [SerializeField]
    private List<GameObject> p_bullet;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        p_bullet = new List<GameObject>();
    }


    public Bullet BulletPoolInstantiate(float bulletLife)
    {
        if (p_bullet.Count > 0)
        {
            foreach (var bullet in p_bullet)
            {
                if (!bullet.activeInHierarchy)
                {
                    //bulletObj = bullet;
                    //bullet.transform.position = transform.position;
                    Bullet _refOldBullet = bullet.GetComponent<Bullet>();
                    _refOldBullet.m_LifeBullet = bulletLife;


                    bullet.SetActive(true);
                    bullet.transform.parent = transform;
                    bullet.transform.localPosition = Vector3.zero;
                    
                    //return temp_bullet;
                    return _refOldBullet;
                }

            }

        }
        GameObject temp_bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        p_bullet.Add(temp_bullet);

        Bullet _refBullet = temp_bullet.GetComponent<Bullet>();
        _refBullet.m_LifeBullet = bulletLife;

        temp_bullet.SetActive(true);
        temp_bullet.transform.parent = transform;
        temp_bullet.transform.localPosition = Vector3.zero;

        return _refBullet;
    }


    public void DestroyBulletPool(Bullet bullet)
    {
        bullet.gameObject.SetActive(false);
        bullet.gameObject.transform.position = Vector3.zero;
    }

    public void DestroyAllBullets()
    {
        foreach(var b in p_bullet)
        {
            b.SetActive(false);
        }
    }
}
