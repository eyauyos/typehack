﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class LevelManager : MonoBehaviour
{
    public Button PauseBtn;
    public GameObject PausePanel;

    public Button ResumeBtn;
    public Button RestartBtn;


    public GameObject WinPanel;
    public Button WinRestartBtn;
    public GameObject GameOverPanel;


    public Player player;
    public GameObject bossController;
    public BossBeh bossBeh;
    public PatternController patternController;
    public UIPlayerController uIPlayerController;


    [Header("Intro")]
    public LevelTitle Tittle;
    public DialogController Dialog;
    public bool dialogActive=true;

    public AudioSource BackgroundNormal;
    public AudioSource BackgroundHigh;


    // Start is called before the first frame update
    void Start()
    {
        player.gameObject.SetActive(false);
        bossController.gameObject.SetActive(false);

        player.canvasUI.SetActive(true);

        PauseBtn.onClick.AddListener(() =>
        {
            PausePanel.SetActive(true);
            AppManager.Instance.PauseGame();
        });

        ResumeBtn.onClick.AddListener(() =>
        {
            PausePanel.SetActive(false);
            AppManager.Instance.ResumeGame();
        });

        RestartBtn.onClick.AddListener(() =>
        {
            RestartGame();
            WinPanel.SetActive(false);
            GameOverPanel.SetActive(false);
            PausePanel.SetActive(false);
        });

        WinRestartBtn.onClick.AddListener(() =>
        {
            RestartGame();
            WinPanel.SetActive(false);
            GameOverPanel.SetActive(false);
            PausePanel.SetActive(false);
        });

        InputAttack.OnBossDefeat += BossDefeated;

        Player.OnGameOver += GameOver;



    }

    private void Update()
    {
        if (Dialog.isFinished&&dialogActive)
        {
            player.gameObject.SetActive(true);
            bossController.gameObject.SetActive(true);

            dialogActive = false;
        }
    }


    //private void 
    public void PauseActions()
    {
        player.gameObject.SetActive(false);
        bossController.gameObject.SetActive(false);
    }



    public void ResumeActions()
    {
        player.gameObject.SetActive(true);
        bossController.gameObject.SetActive(true);
    }
    private void BossDefeated()
    {
        //AppManager.Instance?.PauseGame();
        BackgroundHigh.pitch = 0.5f;
        PauseActions();
        WinPanel.SetActive(true);
    }

    private void GameOver()
    {
        //AppManager.Instance?.PauseGame();
        BackgroundNormal.DOKill();
        BackgroundNormal.pitch = 0.5f;
        PauseActions();
        GameOverPanel.SetActive(true);
        player.canvasUI.SetActive(false);
    }

    private void RestartGame()
    {
        BulletPool.Instance.DestroyAllBullets();
        //player.RestartPlayerStats();
        //AppManager.Instance?.ResumeGame();
        SceneManager.LoadScene("Game");
        
    }

    private void OnDestroy()
    {
        InputAttack.OnBossDefeat -= BossDefeated;

        Player.OnGameOver -= GameOver;
    }

}
