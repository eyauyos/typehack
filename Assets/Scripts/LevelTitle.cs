﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
public class LevelTitle : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI titleText;
    [TextArea]
    public string title;
    [SerializeField] GameObject panelFade;

    private IEnumerator coroutine;
    [SerializeField] public float delay;
    [SerializeField] public float dissapearingDuration;
    void OnEnable()
    {
        titleText.text = title;
        coroutine = waitForDissapear(delay, dissapearingDuration);
        StartCoroutine(coroutine);
    }
    void Start()
    {
        titleText.text = title;
        coroutine = waitForDissapear(delay, dissapearingDuration);
        StartCoroutine(coroutine);
    }
    IEnumerator waitForDissapear(float delay, float dissapearingDuration) {
        yield return new WaitForSeconds(delay);
        DissapearTitle(dissapearingDuration);
    }
    void DissapearTitle(float dissapearingDuration) {
        GetComponent<CanvasGroup>().DOFade(0.0f, dissapearingDuration).onComplete+=()=>
        {
            gameObject.SetActive(false);
        };
    }

    private void OnDisable()
    {
        GetComponent<CanvasGroup>().DOKill();
        StopAllCoroutines();
        GetComponent<CanvasGroup>().alpha = 1;
    }
}
