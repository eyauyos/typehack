﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPController : MonoBehaviour
{
    [Header("Player stats")]
    [SerializeField] float moveSpeed = 10f;
    public bool active;
    private Rigidbody2D rb2D;
    private Vector3 movVector;
    private float deltaX;
    private float deltaY;
    private Animator anim;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(active)
        MoveAxis();
    }

    private void FixedUpdate()
    {
        if(active)
        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f || Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0.5f)
        {
            movVector = transform.position + new Vector3(deltaX, deltaY).normalized * Time.fixedDeltaTime * moveSpeed;
            rb2D.MovePosition(movVector);
        }
    }

    private void MoveAxis()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f || Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f)
        {
            deltaX = Input.GetAxis("Horizontal");
            deltaY = Input.GetAxis("Vertical");


        }

        anim.SetFloat("x", Input.GetAxis("Horizontal"));
        anim.SetFloat("y", Input.GetAxis("Vertical"));

        if ((Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.DownArrow)))
        {
            anim.SetFloat("y", 0);
        }

        if ((Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)))
        {
            anim.SetFloat("x", 0);
        }
    }
}
