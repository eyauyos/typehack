﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class Player : MonoBehaviour
{
    public LevelManager levelManager;

    public GameObject canvasUI;
    public AudioSource audioSource;
    public AudioClip damageSFX;
    [Header("Player stats")]
    public TextMeshProUGUI m_LifeUI;

    [Header("Player stats")]
    [SerializeField] float moveSpeed = 10f;
    public int m_Life = 10;
    public Transform LifesContainer;
    private List<GameObject> LifeImages= new List<GameObject>();

    private Rigidbody2D rb2D;
    private Vector3 movVector;
    private float deltaX;
    private float deltaY;
    
    [Header("Power Up Commands")]
    public string m_DefenseCommand;
    private int indexDef;
    private bool def;
    private string defenseCurrent;
    public string m_DesaparitionCommand;

    public TextMeshProUGUI InputText;
    [Header("Shield of player")]
    public ShieldPlayer m_shieldPlayer;
    public float shieldCD;
    public float shieldDuration;
    private bool ShieldEnable;
    public AudioClip activeShieldSFX;
    public AudioClip disableShieldSFX;

    [Header("SlowTime Power Up")]
    public AudioClip slowTimeSFX;
    //public AudioClip resumeTimeSFX;
    [Range(0, 2f)]
    public float minTimeForEnable;
    [Range(0, 0.5f)]
    public float bulletTimeValue;
    [Range(0,1)]
    public float timeScale;
    [Range(0, 4)]
    public float slowDuration;
    public float maxDuration =4;
    public bool slowEnable;
    public bool slowIsActive;


    [Header("State Player")]
    public StatePlayer m_statePlayer;

    public delegate void Defense();
    public static event Defense OnDefense;


    public delegate void GameOver();
    public static event GameOver OnGameOver;

    public delegate void TakeDamage();
    public static event TakeDamage OnTakeDamage;

    public delegate void SlowTime(float duration);
    public static event SlowTime OnSlowTime;


    private Animator anim;
    private SpriteRenderer spriteRenderer;
    private PolygonCollider2D polygonCollider;

    private Vector3 initialPos;
    
    public enum StatePlayer
    {
        Normal,
        PowerUp

    }

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
        for (int i = 0; i < m_Life; i++)
        {
            GameObject imageLife = Instantiate(LifesContainer.GetChild(0).gameObject, LifesContainer);
            imageLife.SetActive(true);
            LifeImages.Add(imageLife);
        }

    }
    void Start()
    {
        initialPos = transform.position;
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        polygonCollider = GetComponent<PolygonCollider2D>();

        indexDef = 0;

        ShieldEnable = true;
       

        m_LifeUI.text = m_Life.ToString();

        OnDefense += ActiveShield;
        OnTakeDamage += TakeDamageEvent;

        OnSlowTime += SlowTimeEvent;

    }

    private void SlowTimeEvent(float slowTime)
    {
        levelManager.uIPlayerController.DisableSlowTime(slowTime);
    }

    private void TakeDamageEvent()
    {
        StartCoroutine(TakeDamageAnimation(10));
        m_LifeUI.text = m_Life.ToString();
    }

    public void RestartPlayerStats()
    {
        m_Life = 3;
        m_LifeUI.text = " "+m_Life;
        StopAllCoroutines();
        spriteRenderer.color = new Color(1, 1, 1, 1f);
        transform.position = initialPos;
    }

    private void ActiveShield()
    {
        if (ShieldEnable)
        {
            StartCoroutine(ActiveShieldCoroutine());
        }
        else
        {

        }
            
    }

    private IEnumerator ActiveShieldCoroutine()
    {
        audioSource.PlayOneShot(activeShieldSFX);
        ShieldEnable = false;
        m_shieldPlayer.gameObject.SetActive(true);
        polygonCollider.enabled = false;
        levelManager.uIPlayerController.DisabledShield(shieldDuration);
        yield return new WaitForSeconds(shieldDuration-0.5f);
        audioSource.PlayOneShot(disableShieldSFX);
        yield return new WaitForSeconds(0.5f);
        polygonCollider.enabled = true;
        m_shieldPlayer.DisableShield();
        StartCoroutine(ShieldCouldown());
    }

    private IEnumerator ShieldCouldown()
    {
        levelManager.uIPlayerController.EnabledShield(shieldCD);
        yield return new WaitForSeconds(shieldCD);
        ShieldEnable = true;
    }


    private void Update()
    {
        
        MoveAxis();


        if (Input.GetKeyDown(KeyCode.P))
        {
            
            OnDefense?.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift)&&slowEnable)
        {
            
            StartCoroutine(SlowTimeCoroutine());
        }
    }

    private IEnumerator SlowTimeCoroutine()
    {
        audioSource.PlayOneShot(slowTimeSFX);
        slowIsActive = true;
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        OnSlowTime?.Invoke(shieldDuration);
        
        yield return new WaitForSeconds(slowDuration*Time.timeScale);
        slowDuration = 0;
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02F;
        slowEnable = false;
        slowIsActive = false;
        //audioSource.PlayOneShot(resumeTimeSFX);
    }

    private void FixedUpdate()
    {
        if (!Input.GetKey(KeyCode.Mouse1))
        {
            if (Math.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f || Math.Abs(Input.GetAxisRaw("Vertical")) > 0.5f)
            {
                movVector = transform.position + new Vector3(deltaX, deltaY).normalized * Time.fixedDeltaTime * moveSpeed;
                rb2D.MovePosition(movVector);
            }
        }
        
    }

    private void ReadInput()
    {
        if (Input.anyKeyDown)
        {
            if (Input.inputString.Length > 0)
            {
                ValidatePowerUp(Input.inputString[0]);
            }
        }
    }


    private void ValidatePowerUp(char c)
    {
        if (m_statePlayer == StatePlayer.PowerUp)
        {
            m_statePlayer = StatePlayer.Normal;
            InputText.text = "";
        }

        {
            ValidateDefense(c);
           
            if (def)
            {
                InputText.text += Input.inputString[0];
            }
            else
            {
                InputText.text = "";

            }
        }
        
        

    }

    private void ValidateDefense(char c)
    {
        defenseCurrent = m_DefenseCommand.ToLower();

        if (c == defenseCurrent[indexDef])
        {
            indexDef++;

            if (indexDef == defenseCurrent.Length)
            {
                indexDef = 0;
                Debug.Log("correct defense");
                m_statePlayer = StatePlayer.PowerUp;
                OnDefense?.Invoke();
            }
            def = true;
        }
        else
        {
            indexDef = 0;
            def = false;
        }
    }



    private void MoveAxis()
    {
        if(Math.Abs(Input.GetAxis("Horizontal"))>0.1f || Math.Abs(Input.GetAxis("Vertical")) > 0.1f)
        {
            deltaX = Input.GetAxis("Horizontal");
            deltaY = Input.GetAxis("Vertical");

            
        }

        anim.SetFloat("x", Input.GetAxis("Horizontal"));
        anim.SetFloat("y", Input.GetAxis("Vertical"));

        if ((Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.DownArrow)))
        {
            anim.SetFloat("y", 0);
        }

        if ((Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)))
        {
            anim.SetFloat("x", 0);
        }
    }

    private IEnumerator TakeDamageAnimation(int count)
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.3f*Time.timeScale);
        spriteRenderer.color = Color.white;
        while (count > 0)
        {
            spriteRenderer.color = new Color(1, 1, 1, 0.5f);
            polygonCollider.enabled = false;
            yield return new WaitForSeconds(0.1f * Time.timeScale);
            spriteRenderer.color = new Color(1, 1, 1, 1f);
            yield return new WaitForSeconds(0.1f * Time.timeScale);
            count--;
        }
        polygonCollider.enabled = true;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Damage"))
        {
            audioSource.pitch = UnityEngine.Random.Range(1, 1.5f);
            audioSource.PlayOneShot(damageSFX);
            m_Life--;
            if (m_Life >= 0)
            {
                Destroy(LifeImages[m_Life]);
            }
            
            if (m_Life < 3)
            {
                levelManager.BackgroundNormal.DOPitch(1.8f,5f);
            }

            if (m_Life < 0)
            {
                m_Life = 0;
            }
                OnTakeDamage?.Invoke();
            if (m_Life <= 0)
            {
                m_Life = 0;
                OnGameOver?.Invoke();
            }
        }
    }


    private void OnDestroy()
    {
        OnDefense -= ActiveShield;
        OnTakeDamage -= TakeDamageEvent;

        OnSlowTime -= SlowTimeEvent;
    }
    #region METODOS COMENTADOS


    //private void ValidateDesaparition(char c)
    //{
    //    desaparitionCurrent = m_DesaparitionCommand.ToLower();

    //    if (c == desaparitionCurrent[indexDes])
    //    {
    //        indexDes++;

    //        if (indexDes == desaparitionCurrent.Length)
    //        {
    //            indexDes = 0;
    //            Debug.Log("correct desaparition");
    //            m_statePlayer = StatePlayer.PowerUp;
    //            OnDesaparition?.Invoke();
    //        }
    //        des = true;
    //    }
    //    else
    //    {
    //        indexDes = 0;
    //        des = false;
    //    }
    //}


    //private void ValidateInputString(char c)
    //{
    //    currentWord = words[indexWord].ToLower();

    //    if (c == currentWord[indexChar])
    //    {
    //        indexChar++;
    //        InputField.text += c;
    //        Debug.Log("correct");

    //        if (indexChar == currentWord.Length)
    //        {
    //            AccWords.text += words[indexWord] + " ";
    //            indexWord++;
    //            PointCount.text = "" + indexWord;
    //            InputField.text = "";
    //            indexChar = 0;
    //        }
    //    }
    //    else
    //    {
    //        indexChar = 0;
    //        InputField.text = "";
    //        Debug.Log("error");
    //    }
    //}
    #endregion
}
