﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPlayer : MonoBehaviour
{
    public AudioSource shieldSFX;
    public AudioClip bulletSFX;
    public LevelManager levelManager;
    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    private void OnEnable()
    {
        //Invoke("DisableShield", 0.8f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Bullet bullet = null;
        if (collision.gameObject.CompareTag("Damage"))
        {
            shieldSFX.pitch = Random.Range(1, 1.5f);
            shieldSFX.PlayOneShot(bulletSFX);

            if (levelManager.player.slowDuration < levelManager.player.maxDuration&&!levelManager.player.slowIsActive)
            {
                levelManager.player.slowDuration += levelManager.player.bulletTimeValue;
                levelManager.uIPlayerController.SumSlowTime(levelManager.player.bulletTimeValue);
                if (levelManager.player.slowDuration > levelManager.player.minTimeForEnable)
                {
                    levelManager.player.slowEnable = true;
                }
            }
            
            if (collision.gameObject.TryGetComponent(out bullet))
            {
                collision.gameObject.GetComponent<Bullet>().DestroyBulletPool();
                Debug.Log("defendiendo");
            }
        }
    }

    public void DisableShield()
    {
        gameObject.SetActive(false);
    }
}
