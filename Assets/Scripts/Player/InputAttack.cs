﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InputAttack : MonoBehaviour
{

    public delegate void BossDefeat();
    public static event BossDefeat OnBossDefeat;

    public delegate void CorrecString();
    public static event CorrecString OnCorrecString;

    private string currentString;
    private List<string> hackStrings;
    private int charIndex=0;
    public int stringIndex = 0;

    private bool complete;
    private bool delay;


    public LevelManager levelManager;
    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    private void Start()
    {
        hackStrings = levelManager.uIPlayerController.HackStrings;

        levelManager.uIPlayerController.m_Placeholder.text = hackStrings[0];

        OnCorrecString += TakingDamageBoss;
    }

    private void TakingDamageBoss()
    {
        levelManager.bossBeh.TakingDamageCoroutine();
    }

    void Update()
    {
        ReadInput();
    }

    private void ReadInput()
    {
        if (levelManager.player.slowIsActive)
        {
            if (Input.anyKeyDown && !complete)
            {
                if (Input.inputString.Length > 0)
                {
                    ValidateInput(Input.inputString[0]);
                }
            }
        }
            
    }

    private void ValidateInput(char c)
    {
        if (stringIndex < hackStrings.Count)
            currentString = hackStrings[stringIndex].ToLower();

        if (c == currentString[charIndex]&& charIndex < currentString.Length)
        {
            charIndex++;
            if(!delay)
                levelManager.uIPlayerController.m_InputAttack.text += c;

            if (levelManager.uIPlayerController.m_InputAttack.text== currentString)
            {
                StartCoroutine(CorrectString());                        

            }
        }
        else
        {
            charIndex = 0;
            levelManager.uIPlayerController.m_InputAttack.text ="";

        }

    }

    private IEnumerator CorrectString()
    {
        OnCorrecString?.Invoke();
        charIndex = 0;
        Debug.Log("correct string");

        stringIndex++;
        delay = true;
        yield return new WaitForSeconds(1.5f);
        delay = false;
        levelManager.uIPlayerController.m_InputAttack.text = "";

        if (hackStrings.Count - stringIndex < 3)
        {
            levelManager.BackgroundNormal.DOPitch(1.8f,2f);
        }

        if (stringIndex >= hackStrings.Count)
        {
            OnBossDefeat?.Invoke();
            complete = true;
            Debug.Log("Boss defeated");
        }
        else
            levelManager.uIPlayerController.m_Placeholder.text = hackStrings[stringIndex];
    }
    private void OnDestroy()
    {
        OnCorrecString -= TakingDamageBoss;
    }

}
