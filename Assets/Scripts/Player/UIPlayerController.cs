﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIPlayerController : MonoBehaviour
{

    public LevelManager levelManager;
    public TextMeshProUGUI m_PlayerLifeCount;


    [Header("Shield UI")]
    public Image ShieldUI;
    private Color enableShieldColor;
    private Color disabledShieldColor;


    [Header("SlowTime UI")]
    public Slider slowBar;
    public Color enableSlowPowerUp;
    public Color disableSlowPowerUp;
    public Color initialColor;

    [Header("Input Attack")]
    public TextMeshProUGUI m_InputAttack;
    public TextMeshProUGUI m_Placeholder;
    public List<string> HackStrings;
    
    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();


        enableShieldColor = ShieldUI.color;
        disabledShieldColor = new Color(enableShieldColor.r, enableShieldColor.g, enableShieldColor.b, 0.5f);
    }
    private void Start()
    {
        initialColor = slowBar.fillRect.gameObject.GetComponent<Image>().color;
    }
    public void SumSlowTime(float value)
    {
        float time = value / levelManager.player.maxDuration;
        slowBar.value += time;
        if (levelManager.player.slowDuration > levelManager.player.minTimeForEnable)
        {
            slowBar.fillRect.gameObject.GetComponent<Image>().color = enableSlowPowerUp;
        }

        
    }

    public void DisableSlowTime(float slowDuration)
    {
        slowBar.fillRect.gameObject.GetComponent<Image>().DOColor(disableSlowPowerUp, slowDuration /2* Time.timeScale).onComplete += () =>
        {
            slowBar.fillRect.gameObject.GetComponent<Image>().color = initialColor;
        };
        slowBar.DOValue(0, levelManager.player.slowDuration * Time.timeScale);
    }


    public void DisabledShield(float shieldDuration)
    {
        ShieldUI.DOFillAmount(0, shieldDuration).onComplete+=()=>
        {
            ShieldUI.color = disabledShieldColor;
        };
    }
    public void EnabledShield(float shieldCooldown)
    {
        
        ShieldUI.DOFillAmount(1, shieldCooldown).onComplete+=()=>
        {
            ShieldUI.color = enableShieldColor;
        };
        
    }
}
