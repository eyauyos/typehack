﻿using System.Collections;
using UnityEngine;

public class Pattern1 : MonoBehaviour
{
    //public static Canon Instance;

    public float rotationVel;
    public float lifeBullet;
    public float freqBullet;

    // Start is called before the first frame update

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        transform.localEulerAngles= new Vector3(0, 0, 0);
        StartCoroutine(Fire01());

        
    }

    // Update is called once per frame
    void Update()
    {
     
        transform.localEulerAngles += new Vector3(0, 0,Time.deltaTime* rotationVel); //Espiral

    }

    private IEnumerator Fire01()
    {
        while (true)
        {
            Bullet temp_Bullet = BulletPool.Instance.BulletPoolInstantiate(lifeBullet);
            temp_Bullet.transform.position = transform.position+transform.right*Random.Range(0.8f,1);
            temp_Bullet.m_Rbd.AddForce(transform.right * 5, ForceMode2D.Impulse);
            yield return new WaitForSeconds(freqBullet);
        }
    }

    private void OnDisable()
    {
        transform.localEulerAngles = new Vector3(0, 0, 0);
        StopAllCoroutines();
    }

}
