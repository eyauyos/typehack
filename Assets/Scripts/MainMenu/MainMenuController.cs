﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenuController : MonoBehaviour
{
    public Button StartGameBtn;

    public AudioSource audioSource;

    public AudioClip MainMenuSFX;


    public List<Image> SFXImages;
    public Material sfxLineS;
    void Start()
    {
        //StartGameBtn.onClick.AddListener(StartGame);
        audioSource.clip = MainMenuSFX;
        audioSource.loop = true;
        audioSource.Play();

        //SFX();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartGame();
        }
    }

    private void SFX()
    {
        
        //foreach (var sfx in SFXImages)
        //{
        //    sfx.DOFade(0, 2).onComplete+=()=>
        //    {
        //        sfx.DOFade(1, 2);
        //    };
        //}

        LineSFX();

    }

    private void LineSFX()
    {
        //while (true)
        {
            sfxLineS.DOFloat(45, "_Angle", 0.5f).onComplete += () =>
            {
                sfxLineS.DOFloat(-45, "_Angle", 0.5f).onComplete+=()=>
                {
                    LineSFX();
                };
            };
            //yield return null;
        }
        
    }

    private void StartGame()
    {
        SceneManager.LoadSceneAsync("CreationScene");
    }

}
