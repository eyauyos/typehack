﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    [SerializeField] GameObject LevelTitle;
    [SerializeField] GameObject Dialog1;
    [SerializeField] GameObject Chat;
    [SerializeField] GameObject Dialog2;
    [SerializeField] GameObject ChatWindow;
    [SerializeField] GameObject Code;
    [SerializeField] AudioClip ambientMusic;
    private LevelTitle titleManager;
    DialogController dialog1Controller;
    bool oneTimeDialog1Controller = true;
    DialogController dialog2Controller;
    bool oneTimeDialog2Controller = true;
    ChatController chatController;
    bool oneTimeChatController = true;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.Stop();
        audioSource.clip = ambientMusic;
        audioSource.loop = true;
        audioSource.Play();
        titleManager = LevelTitle.GetComponent<LevelTitle>();
        dialog1Controller = Dialog1.GetComponent<DialogController>();
        chatController = Chat.GetComponent<ChatController>();
        dialog2Controller = Dialog2.GetComponent<DialogController>();
        StartCoroutine(StartOpening());

    }

    void Update()
    {
        if(dialog1Controller.isFinished) {
            Chat.SetActive(true);
        }
        if(chatController.isFinished && oneTimeChatController) {
            StartCoroutine(WaitAndOpenDialog2());
        }
        if(dialog2Controller.isFinished) {
            ChatWindow.SetActive(false);
            Code.SetActive(true);
        }

    }
    IEnumerator StartOpening() {
        LevelTitle.SetActive(true); 
        yield return new WaitForSeconds(titleManager.delay + titleManager.dissapearingDuration);
        Dialog1.SetActive(true);            
    }

    IEnumerator WaitAndOpenDialog2() {
        yield return new WaitForSeconds(1.5f);
        Dialog2.SetActive(true);
        oneTimeChatController = false;
    }
}
